package com.example.sergio.examen;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.sergio.examen.BookSearch.Activities.BookListActivity;
import com.example.sergio.examen.Firebase.FirebaseMain;
import com.example.sergio.examen.GMap.LiteDemoActivity;
import com.example.sergio.examen.GMap.MyLocationDemoActivity;
import com.example.sergio.examen.SQLite.TareasSQLiteActivity;


public class MainActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        agregarToolbar();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (navigationView != null) {
            prepararDrawer(navigationView);
        }
    }
    private void agregarToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.drawer_toggle);
            ab.setDisplayHomeAsUpEnabled(true);
        }

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void prepararDrawer(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        seleccionarItem(menuItem);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                });

    }
    private void seleccionarItem(MenuItem itemDrawer) {
        switch (itemDrawer.getItemId()) {
            case R.id.item_GoogleMaps:
                startActivity(new Intent(this, LiteDemoActivity.class));
                break;
            case R.id.item_MyLocation:
                startActivity(new Intent(this, MyLocationDemoActivity.class));
                break;
            case R.id.item_CRUDSQLite:
                startActivity(new Intent(this, TareasSQLiteActivity.class));
                break;
            case R.id.item_Booksearch:
                startActivity(new Intent(this, BookListActivity.class));
                break;
            case R.id.item_Firebase:
                startActivity(new Intent(this, FirebaseMain.class));
                break;

        }
    }
}
