package com.example.sergio.examen.Firebase;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sergio.examen.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import static java.lang.System.load;


public class FirebaseMain extends AppCompatActivity {
    private static final String TAGLOG = "firebase-db";

    private RecyclerView lstPredicciones;

    FirebaseRecyclerAdapter mAdapter;


    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.firebase);
        DatabaseReference dbPredicciones =
                FirebaseDatabase.getInstance().getReference()
                        .child("predicciones");

        RecyclerView recycler = (RecyclerView) findViewById(R.id.lstPredicciones);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        mAdapter =
                new FirebaseRecyclerAdapter<Prediccion, PrediccionHolder>(
                        Prediccion.class, R.layout.item_lista, PrediccionHolder.class, dbPredicciones) {

                    @Override
                    public void populateViewHolder(PrediccionHolder predViewHolder, Prediccion pred, int position) {
                        predViewHolder.setFecha(pred.getFecha());
                        predViewHolder.setCielo(pred.getCielo());
                        predViewHolder.setTemperatura(pred.getTemperatura() + "ºC");
                        predViewHolder.setHumedad(pred.getHumedad() + "%");
                    }
                };

        recycler.setAdapter(mAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.cleanup();
    }
}



